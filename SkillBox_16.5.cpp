// SkillBox_16.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int day = buf.tm_mday;

	const int size = 5;	
	int Array[size][size];
	int Sum = 0 ;
	
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			Array[i][j] = i + j;
			std::cout << Array[i][j];
		}
		std::cout << "\n";

	}
	int ArrayIndex = (day % size); 
	for (int t = 0; t < size; ++t)
	{
		Sum += Array[ArrayIndex][t];
	}
	std::cout << ArrayIndex << " Row \n";
	std::cout << Sum << " Sum of numbers in the Row \n";
}

